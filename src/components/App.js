import React, { Component } from 'react';
import '../componentStyles/App.css';
import NavBar from './NavBar';
import Edit from './Edit';
import Footer from './Footer';


class App extends Component {
  render() {
    return <div>
      <NavBar/>
      <Edit/>
      <Footer/>
    </div>
  }
}

export default App;

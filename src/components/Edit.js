import React, { Component } from 'react';
import EmailEditor from 'react-email-editor'
import styled, { injectGlobal } from 'styled-components';
import '../componentStyles/Edit.css';
import sample from '../sample.json';

injectGlobal`
  html, body {
    margin: 0;
    padding: 0;
    height: 100%;
    font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;

  }
`

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  height: 100%;
`
const Bar = styled.div`
  flex: 1;
  background-color: #4169E1;
  color: #FFF;
  padding: 10px;
  margin-bottom: 30px;
  display: flex;
  max-height: 40px;
  font-family: 'Proxima Nova';
  h1 {
    flex: 1;
    font-size: 16px;
    text-align: left;
  }
  button {
    flex: 1;
    padding: 10px;
    margin-left: 10px;
    font-size: 14px;
    font-weight: bold;
    background-color: #000;
    color: #FFF;
    border: 0px;
    max-width: 150px;
    cursor: pointer;
  }
`

class Edit extends Component {
    render() {
        return <Container>
        <Bar>
            <h1 className="bar-header">Let's Get Started</h1>
            <button onClick={this.saveDesign} className="save-btn">Save Design</button>
            <button onClick={this.exportHtml} className="export-btn">Export HTML</button>
        </Bar>
        <EmailEditor
            ref={editor => this.editor = editor}
            onLoad={this.onLoad}
            className="email-editor-content"
            
        />
        </Container>
      }
     
      exportHtml = () => {
        this.editor.exportHtml(data => {
          const { design, html } = data
          console.log('exportHtml', html)
          console.log('design', design)
          alert('exporting html')
        })
      }

      saveDesign = () => {
        this.editor.saveDesign(design => {
          console.log('saveDesign', design)
          alert('Design JSON has been logged in your console')
        })
      }

      onLoad = () => {
          this.editor.loadDesign(sample)
      }
}

export default Edit;
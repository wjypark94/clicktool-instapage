import React, {Component} from 'react';
import '../componentStyles/Footer.css';

class Footer extends Component {
    render(){
        return (
            <div className="footer">
            <div className="footer-links">
                <a href="mailto:contact@clicktool.com">
                    <i className="icon fa fa-envelope"></i>
                </a>
                <a href="https://www.clicktool.com">
                    <i className="icon fa fa-github-alt" aria-hidden="true"></i>
                </a>
                <a href="https://www.linkedin.com/clicktool">
                    <i className="icon fa fa-linkedin" aria-hidden="true"></i>
                </a>
            </div>

            <p className="copyright">
                <i className="copyright-icon fa fa-copyright"></i>
                2018 ClickTool's Copyright 
            </p>
        </div>
        )
    }
}

export default Footer;
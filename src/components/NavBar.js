import React, { Component } from 'react';
import '../componentStyles/NavBar.css';

class NavBar extends Component {
    render(){
        return (
            <nav className="Nav">
                <ul className="container">
                    <li className="nav-name">
                        ClickTool Page Loader
                    </li>
                    <li className="nav-item">
                        SERVICES
                    </li>
                    <li className="nav-item">
                        REVIEWS
                    </li>
                    <li className="nav-item">
                        ABOUT
                    </li>
                    <li className="nav-item2">
                        RESOURCES
                    </li>
                    <li className="nav-item">
                        LOGIN
                    </li>
                    <li className="nav-signup">
                        SIGNUP
                    </li>
                </ul>
            </nav>
        )
    }
}

export default NavBar;